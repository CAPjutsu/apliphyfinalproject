﻿using UnityEngine;
using System.Collections;



public class CustomVector  {
    public float x, y, z;

     public CustomVector(float Px, float Py, float Pz)
    {
        Px = x;
        Py = y;
        Pz = z;
    }

    public CustomVector AddVector(CustomVector Vector1 , CustomVector Vector2)
    {
       float rX = Vector1.x + Vector2.x;
       float rY = Vector1.y + Vector2.y;
       float rZ = Vector1.z + Vector2.z;
       CustomVector ResultVector = new CustomVector(rX,rY,rZ);
       return ResultVector;
    }

    public CustomVector SubVector(CustomVector Vector1, CustomVector Vector2)
    {
        float rX = Vector1.x - Vector2.x;
        float rY = Vector1.y - Vector2.y;
        float rZ = Vector1.z - Vector2.z;
        CustomVector ResultVector = new CustomVector(rX, rY, rZ);
        return ResultVector;
    }

    public float Distance(CustomVector Vector1, CustomVector Vector2)
    {
        float rX, rY, rZ, Dist;

        rX = Mathf.Sqrt(Vector1.x - Vector2.x);
        rY = Mathf.Sqrt(Vector1.y - Vector2.y);

        /*Dist = 0; //placeholder*/
        Dist = rX + rY;

        return Dist;
    }

    public CustomVector NormalizedVector(CustomVector VectorX)
    {
        float nX = (VectorX.x / VectorMagnitude(VectorX));
        float nY = (VectorX.y / VectorMagnitude(VectorX));
        float nZ = (VectorX.z / VectorMagnitude(VectorX));
        CustomVector ResultVector = new CustomVector(nX, nY, nZ);
        return ResultVector;
    }

    public float VectorMagnitude(CustomVector vectorX)
    {
        float Mag = 0.0f;
        Mag = Mathf.Sqrt(((vectorX.x * vectorX.x) + (vectorX.y * vectorX.y) + (vectorX.z * vectorX.z)));
        return Mag;
    }


    public void DotVector()
    {
        
    }

    public void CrossVector()
    {

    }

}

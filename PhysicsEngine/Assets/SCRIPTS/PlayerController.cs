﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float[] AccelerationFloat;
    public CustomVector AccelerationX = new CustomVector(0,0,0);
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            PhysicsBodies PBodiesInstance = GetComponent<PhysicsBodies>();
            PBodiesInstance.Data.Velocity.x = 0.0f;
            IncreaseAcceleration();
            PBodiesInstance.AddAcceleration(AccelerationX);
        }

        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            PhysicsBodies PBodiesInstance = GetComponent<PhysicsBodies>();
            PBodiesInstance.Data.Velocity.x = 0.0f;
            DecreaseAcceleration();
            PBodiesInstance.AddAcceleration(AccelerationX);
        }
        else
        {
            PhysicsBodies PBodiesInstance = GetComponent<PhysicsBodies>();
            ZeroAcceleration();
            PBodiesInstance.AddAcceleration(AccelerationX);
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            PhysicsBodies PBodiesInstance = GetComponent<PhysicsBodies>();
            if (PBodiesInstance.Data.isGrounded)
            {
                PBodiesInstance.Data.isGrounded = false;
                PBodiesInstance.Data.Velocity.y = 10.0f;

            }
        }
        else
        {
            PhysicsBodies PBodiesInstance = GetComponent<PhysicsBodies>();
            ZeroAcceleration();
            PBodiesInstance.AddAcceleration(AccelerationX);
        }


    }

    void IncreaseAcceleration()
    {
        AccelerationX.x = this.AccelerationFloat[0];
        AccelerationX.y = this.AccelerationFloat[1];
        AccelerationX.z = this.AccelerationFloat[2];
    }

    void DecreaseAcceleration()
    {
        AccelerationX.x = -this.AccelerationFloat[0];
        AccelerationX.y = -this.AccelerationFloat[1];
        AccelerationX.z = -this.AccelerationFloat[2];
    }

    void ZeroAcceleration()
    {
        AccelerationX.x = 0.0f;
        AccelerationX.y = 0.0f;
        AccelerationX.z = 0.0f;
    }
}

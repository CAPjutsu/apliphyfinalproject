﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ObjectType
{
    public bool isDynamic = false;
    public bool isKinimatic = false;
    public bool isStatic = false;
    public bool Gravity = false;
}
[System.Serializable]
public class ObjectPhysicsVariables
{
    public float Mass;
    public float Force;
    public float Weight;
    public CustomVector Acceleration = new CustomVector(0.0f,0.0f,0.0f);
    public CustomVector Velocity = new CustomVector(0.0f, 0.0f, 0.0f);
    public float GravityConstant = -9.8f;
    public bool isGrounded = false; 

}


public class PhysicsBodies : MonoBehaviour {

    public CustomVector ObjectVector;
    public CustomVector MinBoundingArea = new CustomVector(0.0f,0.0f,0.0f);
    public CustomVector MaxBoundingArea = new CustomVector(0.0f, 0.0f, 0.0f);
    public ObjectType Type;
    public ObjectPhysicsVariables Data;
    BodyList BoundingArea;
    public GameObject BodyList;
    public Vector3 MonoVector;
    public float TopSpeed = 5.0f;

    CustomVector VectorTEST = new CustomVector(0.0f, 0.0f, 0.0f);


    void Start ()
    {
        ObjectVector = ConvertTransformToCustom();
        BodyList = GameObject.FindGameObjectWithTag("BodyList");
        BoundingArea = BodyList.GetComponent<BodyList>();
        BoundingArea.Tracker.Add(this);
	}
	
	
	void Update ()
    {
        SquareBoundCalc();

        if (Type.isStatic == false)
        {
            ObjectVector = ConvertTransformToCustom();


            AddAcceleration(VectorTEST);
            if (!Input.anyKey)
            {
                DecreaseVelocity();
            }
            //else
            AddVelocity();
            Debug.Log(Data.Velocity.x);
            ConvertCustomToMono();
            transform.position += MonoVector * Time.deltaTime;
        }
        

        //transform.position += new Vector3(Data.Velocity.x,Data.Velocity.y,Data.Velocity.z) * Time.deltaTime;
        //this.ObjectVector =  this.ObjectVector.AddVector(this.ObjectVector, Data.Velocity);
        //Debug.Log(MaxBoundingArea.x + " " + MaxBoundingArea.y);
        //Debug.Log(transform.position.x - (transform.localScale.x / 2))
    }

    void FixedUpdate()
    {
        
        CollisionCheck();
        Gravity();
    }


    void DataSetup()
    {
        Data.Weight = Data.GravityConstant * Data.Mass;
    }

    void Gravity()
    {
        if (Data.isGrounded == false)
        {
            Data.Acceleration.y = Data.GravityConstant * Time.deltaTime;
            Data.Velocity.y += Data.Acceleration.y;
        }
        if (Data.isGrounded == true)
        {
            Data.Acceleration.y = 0.0f;
            Data.Velocity.y = 0.0f;
        }
    }

    void CollisionCheck()
    {
        PhysicsBodies BodyB = null;
        BodyB = BodiesCheck();
        if (BodyB != null)
        {
            if (BodyB.Type.isStatic == true)
            {
                bool check = isGroundedCheck(BodyB);
                if (check == true)
                    Data.isGrounded = true;
                if (check == false)
                    Data.isGrounded = false;
            }
            if (BodyB.Type.isKinimatic == true)
            {
                bool check = isGroundedCheck(BodyB);
                if (check == true)
                    Data.isGrounded = true;
                if (check == false)
                    Data.isGrounded = false;
            }
        }
        else
            return;
    }


    void DecreaseVelocity()
    {
        if (Data.Velocity.x > -0.6f && Data.Velocity.x < 0.6f)
        {
            Data.Velocity.x = 0.0f;
        }

       else if (Data.Velocity.x > 0.05f)
        {
            Data.Velocity.x -= 1.0f;
        }
        else
        //if (Data.Velocity.x < -0.05f)
        {
            Data.Velocity.x += 1.0f;
        }


       
        
    }

    void AddVelocity()
    {
        float velocityX = Data.Acceleration.x * Time.deltaTime;
        Data.Velocity.x += velocityX;

        if (Data.Velocity.x >= TopSpeed)
        {
            Data.Velocity.x = TopSpeed;
        }


        if (Data.Velocity.x <= -TopSpeed && Data.Velocity.x != 0)
        {
            Data.Velocity.x = -TopSpeed;
        }
        
        else
            DecreaseVelocity();
    }

    public void AddAcceleration(CustomVector VectorX)
    {
        Data.Acceleration.x += VectorX.x;
        Data.Acceleration.y += VectorX.y;
        Data.Acceleration.z += VectorX.z;
    }

    void ConvertCustomToMono()
    {
        MonoVector.x = Data.Velocity.x;
        MonoVector.y = Data.Velocity.y;
        MonoVector.z = Data.Velocity.z;
    }
   
    public CustomVector ConvertTransformToCustom() //converts transform.position from mono vector to our own custom vector
    {
        
        float cX, cY, cZ;
        cX = transform.position.x;
        cY = transform.position.y;
        cZ = transform.position.z;
        CustomVector ResultVector = new CustomVector (cX,cY,cZ);
        return ResultVector;
    }


    void SquareBoundCalc()
    {
        float minX = this.transform.position.x - (transform.localScale.x / 2);
        float maxX = this.transform.position.x + (transform.localScale.x / 2);
        float minY = this.transform.position.y - (transform.localScale.y / 2);
        float maxY = this.transform.position.y + (transform.localScale.y / 2);

        this.MinBoundingArea.x = minX;
        this.MinBoundingArea.y = minY;
        this.MaxBoundingArea.y = maxY;
        this.MaxBoundingArea.x = maxX;
    }


    PhysicsBodies BodiesCheck()
    {
        PhysicsBodies BodyReturn = null;
       
        foreach (PhysicsBodies BodyB in BoundingArea.Tracker)
        {
            if (BodyB != this)
            {
                
                if (MinMaxCheck(BodyB) == true)
                {

                    BodyReturn = BodyB;
                    return BodyReturn;
                }
                else
                {
                  
                    BodyReturn = null;
                    this.Data.isGrounded = false;
                    //return BodyReturn;
                }
            }
        }
        return null;
    }



    public bool MinMaxCheck(PhysicsBodies BodyB)
    {
        if (MaxBoundingArea.x < BodyB.MinBoundingArea.x || MinBoundingArea.x > BodyB.MaxBoundingArea.x)
            return false;
        if (MaxBoundingArea.y < BodyB.MinBoundingArea.y || MinBoundingArea.y > BodyB.MaxBoundingArea.y)
            return false;
        else
            return true;
    }

    public bool isGroundedCheck(PhysicsBodies BodyB)
    {


        if (MinBoundingArea.y < BodyB.MaxBoundingArea.y + 0.10f || MinBoundingArea.x < BodyB.MaxBoundingArea.x + 0.10f)
        {

            return true;
        }
        /*
        if (MinBoundingArea.x < BodyB.MaxBoundingArea.x + 0.10f)
        {
            return true;
        }*/
        else
        {

            return false;
        }
    }

}

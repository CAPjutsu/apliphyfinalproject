﻿using UnityEngine;
using System.Collections;

public class Animate : MonoBehaviour {
    Animator anim;
    SpriteRenderer sr;
    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            anim.SetInteger("State", 1);
            sr.flipX = false;
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            anim.SetInteger("State", 1);
            sr.flipX = true;
        }
        else
            anim.SetInteger("State", 0);
            
        


    }
}
